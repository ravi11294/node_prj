const express = require('express')
const user_controller = require("../controllers/users")
const router = express.Router();
router.get('/',user_controller.users)
router.get('/new',user_controller.new)
router.post('/create',user_controller.create)
router.get('/destroy',user_controller.destroy)
module.exports = router;
