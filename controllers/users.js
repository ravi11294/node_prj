const User = require("../models/user")
const url = require('url');

module.exports = {
    users: async function(req,res){
        let usrs = await User.findAllUsers();
        console.log("ssssssss",usrs)
        res.render('index.ejs', { users: usrs})
        },
    new: function(req,res){
        res.render('new.ejs')
        }
        ,
    create: function(req,res){
            let usr = new User(req.body)
            usr.save()
            res.redirect('/');
        },
    destroy: function(req,res){
            let queryObj= url.parse(req.url,true).query
            console.log(queryObj.id)
            // User.deleteOne({_id:queryObj.id})
            User.remove({_id: queryObj.id.toString()}, function(err) {
              if (err) {
                  console.log("not deleted",err)
              }
              else {
                 console.log("Record deleted.");
              }
              });
            res.redirect('/');
        }
}