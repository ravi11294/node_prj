const express = require('express');
var mongoose = require('mongoose');
// let ejs = require('ejs');
var path = require('path')
var url = `mongodb://${process.env.db_service_name}:27017/${process.env.db_name}`;
const db = mongoose.connection;
const app = express();

mongoose.connect(url, {
  ssl: process.env.mongo_ssl,
    sslValidate: process.env.mongo_ssl_validate,
    sslCA: process.env.mongo_cert,
    useUnifiedTopology: true,
    useNewUrlParser: true
});
db.on('connected', function () {
  console.log("db connected",url)
  let routes = require('./routes')
  app.use('',routes)
  app.set('view engine', 'ejs')
  app.engine('ejs', require('ejs').__express);
  app.use(express.static('public'))
});
app.use(express.urlencoded({extended: true}));

app.listen(3000)
