const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const userSchema = new Schema({
  first_name: {
    type: String,
    required: true,
  },
  last_name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  phone_number: {
    type: String,
    required: true,
  }
}, { timestamps: true })

userSchema.statics.findAllUsers = async function(){
  let res = await this.find({}).exec();
  return res;
}

let User = module.exports = mongoose.model('User',userSchema)